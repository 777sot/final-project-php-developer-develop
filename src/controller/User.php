<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * Class User
 */
class User
{
    protected PDO $connection;

    /**
     * User constructor.
     * @param $host
     * @param $dbname
     * @param $user
     * @param $pass
     * @param int $port
     */
    public function __construct($host, $dbname, $user, $pass, $port = 3306)
    {
        try {
            $this->connection = new PDO(
                "mysql:host=$host; dbname=$dbname; port=$port; charset=UTF8",
                $user,
                $pass
            );
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return bool
     */
    function addUser()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_REQUEST['rout'] === 'user/') {

            $data = [
                'first_name' => $_REQUEST['first_name'],
                'email' => $_REQUEST['email'],
                'password' => password_hash($_REQUEST['password'], PASSWORD_DEFAULT),
                'role' => $_REQUEST['role'],
                'date_created' => date('Y-m-d')
            ];

            if (!empty($data)) {
                $statement = $this->connection->prepare
                ('INSERT INTO 
                     users( 
                           first_name,  
                           email,
                           password,
                           role,
                           date_created
                     ) 
                     VALUES( 
                            :first_name, 
                            :email, 
                            :password, 
                            :role,
                            :date_created
                     )
               ');
                return $statement->execute($data);
            }
        }
    }

    /**
     * @return bool
     */
    function updateUser()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'PUT'
            && $_REQUEST['rout'] === 'user/'
            && $_REQUEST['id']
        ) {

            $data = [
                'id' => $_REQUEST['id'],
                'first_name' => $_REQUEST['first_name'],
                'email' => $_REQUEST['email'],
                'password' => password_hash($_REQUEST['password'], PASSWORD_DEFAULT),
                'role' => $_REQUEST['role'],
                'date_created' => date('Y-m-d')
            ];

            if (!empty($data)) {

                $statement = $this->connection->prepare
                ('UPDATE 
                     users 
                     SET 
                         first_name = :first_name,  
                         email = :email,
                         password = :password,
                         role = :role,
                         date_created = :date_created
                     WHERE 
                          id = :id
                ');
                return $statement->execute($data);
            }
        }
    }

    public function deleteUser($id)
    {
        $id ? $_REQUEST['id'] : $id;

        if ($id && $_SERVER['REQUEST_METHOD'] === 'DELETE' && $_REQUEST['rout'] === 'user/' . $id) {
            return $this->connection->prepare('DELETE FROM users WHERE id = :id')->execute(['id' => $id]);
        }
    }

    public function listUsers()
    {
        if ($_REQUEST['rout'] === 'user/' && $_SERVER['REQUEST_METHOD'] === 'GET' && !isset($_REQUEST['REQUEST_METHOD'])) {
            return $this->connection->query('SELECT * FROM users')->fetchAll();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserId($id)
    {
        if ($id && $_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'user/' . $id) {
            return $this->connection
                ->query('SELECT * FROM users WHERE `id` =' . $id)->fetch();

        }
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getUserByEmail($email)
    {
        if ($email && $_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'user/search/' . $email) {
            return $this->connection
                ->query("SELECT * FROM users WHERE `email` = '$email'")->fetchAll();

        }
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    public function login($email, $password)
    {
        if (!$email && !$password) {
            return false;
        }

        if ($email && $password && $_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'users/login/') {
            return $this->connection
                ->query(" SELECT id, first_name, email, password, role FROM users WHERE `email` = '$email'")->fetch();
        }
    }


    public function logout()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'users/logout/') {
            session_unset();
            session_destroy();
        }
    }

    /**
     * @param $email
     * @param $newPassword
     * @return bool|string
     */
    public function resetPassword($email, $newPassword)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'users/reset_password/' && $email && $newPassword !== null) {

            $stmt = $this->connection
                ->query(" SELECT id, first_name, email, password, role  FROM users WHERE `email` = '$email'")->fetch();

            if ($stmt['email'] === $email) {
                $urlPasswordReset =
                    $_SERVER['REQUEST_SCHEME']
                    . '//' . $_SERVER['HTTP_HOST']
                    . '/users/?id=' . $stmt['id'] . '&first_name=' . $stmt['first_name']
                    . '&email=' . $email . '&password=' . $newPassword;

                $mail = new PHPMailer(true);
                $mail->setLanguage('ru', 'vendor/phpmailer/phpmailer/language/');
                $title = 'Ссылка для смены пароля';
                $body = "Перейдите по ссылке для смены текущего пароля <br><br>
                        <a href='$urlPasswordReset' target='_blank'>Сменить пароль</a><br><br>
                        <span>Скопировать ссылку : </span><br><br>
                        <span>$urlPasswordReset</span>
                ";
                try {
                    $mail->isSMTP();
                    $mail->CharSet = "UTF-8";
                    $mail->SMTPAuth = true;
                    //$mail->SMTPDebug = 1;
                    $mail->Host = 'smtp.mail.ru';
                    $mail->SMTPAuth = true;
                    $mail->Username = '777soten@mail.ru';
                    $mail->Password = 'e1bE4BcYKTfyvQNDLVTV';
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                    $mail->Port = 465;
                    $mail->setFrom('777soten@mail.ru', 'Система оповещения');
                    $mail->addAddress($email);
                    //Content
                    $mail->isHTML(true);
                    $mail->Subject = $title;
                    $mail->Body = $body;
                    $mail->send();
                    return true;
                } catch (Exception $e) {
                    return $mail->ErrorInfo;
                }
            }
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public  function getUserIdForClassFile($id)
    {
        return $this->connection
            ->query('SELECT * FROM users WHERE `id` =' . $id)->fetch();
    }

}
