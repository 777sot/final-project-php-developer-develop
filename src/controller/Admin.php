<?php


class Admin
{
    protected PDO $connection;

    /**
     * Admin constructor.
     */
    public function __construct($host, $dbname, $user, $pass, $port = 3306)
    {
        try {
            $this->connection = new PDO(
                "mysql:host=$host; dbname=$dbname; port=$port; charset=UTF8",
                $user,
                $pass
            );
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    /**
     * @return array|bool
     */
    public function listUsers()
    {
        if ($_SESSION['role'] === 'ADMIN_ROLE'
            && $_REQUEST['rout'] === 'admin/user/'
            && $_SERVER['REQUEST_METHOD'] === 'GET') {

            return $this->connection->query('SELECT * FROM users')->fetchAll();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserId($id)
    {

        if ($id && $_SERVER['REQUEST_METHOD'] === 'GET'
            && $_REQUEST['rout'] === 'admin/user/' . $id
            && $_SESSION['role'] === 'ADMIN_ROLE') {
            return $this->connection
                ->query('SELECT * FROM users WHERE `id` =' . $id)->fetch();
        }
    }

    /**
     * @param $id
     * @return void
     */
    public function deleteUser($id)
    {
        $id = $_REQUEST['id'] ?: $id;

        if ($id && $_SERVER['REQUEST_METHOD'] === 'DELETE'
            && $_REQUEST['rout'] === 'admin/user/' . $id
            && $_SESSION['role'] === 'ADMIN_ROLE') {
            $delete = $this->connection->prepare('DELETE FROM users WHERE id = :id')->execute(['id' => $id]);
        }
        if ($delete === true) {
            header('Location: http://' . $_SERVER['HTTP_HOST'] . '/admin/user/');
        }
    }

    /**
     * @return bool
     */
    public function updateUser()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'PUT'
            && $_REQUEST['rout'] === 'admin/user/'
            && $_SESSION['role'] === 'ADMIN_ROLE') {

            $data = [
                'id' => $_REQUEST['id'],
                'first_name' => $_REQUEST['first_name'],
                'email' => $_REQUEST['email'],
                'password' => password_hash($_REQUEST['password'], PASSWORD_DEFAULT),
                'role' => $_REQUEST['role'],
                'date_created' => date('Y-m-d')
            ];

            if (!empty($data)) {

                $statement = $this->connection->prepare
                ('UPDATE 
                     users 
                     SET 
                         first_name = :first_name,  
                         email = :email,
                         password = :password,
                         role = :role,
                         date_created = :date_created
                     WHERE 
                          id = :id
                ');

                $result = $statement->execute($data);

                if ($result === true) {
                    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/admin/user/');
                }

            }
        }
        return false;
    }
}